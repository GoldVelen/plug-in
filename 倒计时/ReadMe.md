<!--
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-11-07 11:30:50
 * @LastEditors: Velen
 * @LastEditTime: 2019-11-07 11:51:21
 -->

# 针对业务

需要获取倒计时的场景；  
需要倒计时动画的场景；  
需要自定义倒计时的场景。  

# 环境依赖

暂时只能运行在浏览器  
**会在下个版本更新依赖包**

# 文件结构

*countDown.js*  主JS文件  
*index.html*  示例html  

# 使用说明

new Count({*condition*})

# 参数说明

*time* String or Number  
必填项：正确的时间格式或时间戳  
*type* Number  
选填项，默认为3：需要的类型数，如时分秒传3，天时分秒传4  
*desc* Boolean  
选填项，默认为false：是否需要转成口语描述，暂未添加此功能  
*isUse* Boolean  
选填项，默认为false：是否需要渲染dom，如是，则会将倒计时渲染至传入的dom  
*dom* Array  
选填项，默认为[]：开启此项取决于isUse是否为true。最多可接受4个参数,参数为元素类型 + 元素类名，如'.dom'或'#dom'。数组内顺序为秒、分、时、天。  
